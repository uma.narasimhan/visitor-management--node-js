const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

//import routes
const appRoute = require('./routes/vmauth');
const database = require('./config/database');


COSMOSDB_CONNSTR="mongodb://visitormanagement.documents.azure.com:10255/visitormanagement"
COSMODDB_USER="visitormanagement"
COSMOSDB_PASSWORD="NfNBSxppV0dq9ebVvrRvafXoM0fEnxOxJQHvXU6gt22wqgvD9N0WGbbeqxoIeCBd4frT8RYrKifRo9T45JyNBw=="

mongoose.connect(COSMOSDB_CONNSTR+"?ssl=true&replicaSet=globaldb", {
  useNewUrlParser: true,auth: {
    user: COSMODDB_USER,
    password: COSMOSDB_PASSWORD
  }
})
.then(() => console.log('Connection to CosmosDB successful'))
.catch((err) => console.error(err));


//handling middle ware
//app.use(express.json());
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

// adding CORS policy

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});


// calling Route middleware

app.use('/api/user',appRoute);

// connecting to the port
app.listen(process.env.port||8080,()=>{
  console.log('server is up and running');
});
