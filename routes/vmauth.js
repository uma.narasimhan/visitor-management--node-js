const router = require('express').Router();
const VMUser = require('../models/VMUser');
const VMOnlineUser = require('../models/VMOnlineUser');
const {registerValidation,onlineregisterValidation} = require('../validation');
const QRCode = require('qrcode')
const nodeMailer = require('nodemailer');
const fs = require('fs');

// user registration without appointment

router.post('/register', async(req,res)=> {

//validation the values

const {error} = registerValidation(req.body);
if(error) return res.status(400).send(error.details[0].message);

// verify if email id already exists
//const emailExists = await VMUser.findOne({email:req.body.email});
//if(emailExists) return res.status(400).send('Email id already exists');

const image = req.body.image;

const data = image.replace(/^data:image\/\w+;base64,/, "");

const buf = new Buffer.from(image, 'base64');

//fs.writeFile('image.png', buf,function(err){
//});



// create new user
const user = new VMUser(
{
  visitorname:    req.body.visitorname,
  vistoremail:   req.body.vistoremail,
  visitormnumber: req.body.visitormnumber,
  visitororgname:req.body.visitororgname,
  persontomeet  : req.body.persontomeet,
  image:  { data: buf, contentType: 'image/png'}
  });

try{
  const savedUser = await user.save();
 
  const transporter = nodeMailer.createTransport({
   host: 'smtp.googlemail.com',
  port: 465,
 secure: true,

   auth: {
       user: process.env.USER_NAME,
       pass : process.env.PASSWORD,
   }
   
  }); 


  const visitorname = user.visitorname;
  const visitoremail = user.vistoremail;
  const visitormnumber = user.visitormnumber;
  const visitororgname = user.visitororgname;
  const persontomeet = user.persontomeet;

  let mailOptions = {
               
  
    from: process.env.SENDER_ADDRESS,  // sender address
    to: visitoremail,
    cc: process.env.LIST_RECEIVERS, // list of receivers
    //bcc:process.env.SENDER_ADDRESS,
    subject: 'Email from ISITA Visitor Management', // Subject line
    text: 'Hello', // plain text body
    html: '<p>Hello from ISITA ,</p></br> </br></br> <p>Below is the visitor details:</p> </br> <img src="cid:myimage"/></br> <p><b>Name of the Visitor:</b>"'+visitorname+'",</p></br> <p><b>Visitor Email:</b> "'+visitoremail+'",</p> </br> <p><b>Visitor Mobile Number:</b>"'+visitormnumber+'",</p> </br><p><b>Visitor Organization Name:</b>"'+visitororgname+'",</p></br><p><b>Person to meet in the Organization::</b>"'+persontomeet+'",</p>',
    attachments: [
      
             {   // data uri as an attachment
                 path: 'data:image/png;base64,aGVsbG8gd29ybGQ=',
                 cid: 'myimage' //same cid value as in the html img src
             }
         ]

  };

transporter.sendMail(mailOptions, (error, info) => {
  if (error) {
      return console.log(error);
  }
  console.log('Message %s sent: %s', info.messageId, info.response);
    res.send(savedUser);
});
  
} catch(err){
  res.status(404).send(err);
}
  } );


// user registration with appointment

router.post('/onlineregister',async(req,res)=>{

const qrcode = req.body.qrcode;
const data = qrcode.replace(/^data:image\/\w+;base64,/, "");
const buf = new Buffer.from(qrcode,'base64');

  //validation the values

const {error} = onlineregisterValidation(req.body);
if(error) return res.status(400).send(error.details[0].message);

  // create new online user
const onlineuser = new VMOnlineUser(
  {
    visitorname : req.body.visitorname,
    visitoremail : req.body.visitoremail,
    visitororgname : req.body.visitororgname,
    visitormnumber  : req.body.visitormnumber,
    meetingstartdate : req.body.meetingstartdate,
    meetingenddate  : req.body.meetingenddate,
    orgpersonname  : req.body.orgpersonname,
    orgpersonemail  : req.body.orgpersonemail,
    department : req.body.department,
    qrcode : { data: buf, contentType: 'image/png'}
           
  } );
   try{
   
       const savedOnlineUser = await onlineuser.save();
       

      
       const transporter = nodeMailer.createTransport({
        host: 'smtp.googlemail.com',
        port: 465,
      secure: true,
            auth: {
            user: process.env.USER_NAME,
            pass : process.env.PASSWORD,
        }
        
       }); 
      
       const Meetingstartdate = onlineuser.meetingstartdate;
       const Meetingenddate = onlineuser.meetingenddate;
       const orgpersonname = onlineuser.orgpersonname;
       const orgpersonemail = onlineuser.orgpersonemail;
       const department = onlineuser.department;
      //const qrcode = QRCode.toDataURL(onlineuser.qrcode);
       
       console.log(Meetingstartdate,Meetingenddate,orgpersonname,orgpersonemail);

       let mailOptions = {
               
 
         from: process.env.SENDER_ADDRESS, // sender address
        to: onlineuser.visitoremail,
        cc: onlineuser.orgpersonemail,
        bcc:process.env.SENDER_ADDRESS,  // list of receivers
        subject: 'Email from ISITA Visitor Management', // Subject line
        text: 'Hello', // plain text body
        //html: '<p>Hello from ISITA ,</p></br> </br></br> <p>Below is your schedule for the meeting:</p> </br> <img src="cid:myimage"/></br> <p><b>Start Date of the Meeting:</b>"'+Meetingstartdate+'",</p></br> <p><b>End Date of the Meeting:</b> "'+Meetingenddate+'",</p> </br> <p><b>Person to meet in the Organization:</b>"'+orgpersonname+'",</p> </br><p><b>Email id of the Organization person:</b>"'+orgpersonemail+'",</p><p><b>Department:</b>"'+department+'",</p>',
        
        
        html: 'Attached is your QRcode for the meeting: ',
        attachments: [
          
                 {   // data uri as an attachment
                     
                    filename:  "QRCODE.jpg",
                   contentType:  'image/jpeg',
                    content: new Buffer.from(req.body.qrcode.split("base64,")[1], "base64"),
                 }
             ]
   
      };
 
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
          return console.log(error);
      }
    console.log('Message %s sent: %s', info.messageId, info.response);
      res.send(savedOnlineUser);
    });
    
   }
    catch(err){
        res.status(404).send(err);
    }
});


router.get('/onlineregister/:qrcode',async(req,res)=>{

  try{
    const User = await VMOnlineUser.findOne({qrcode : req.params.qrcode});
    const Img = await QRCode.toDataURL(User.qrcode);    
   //res.send(Img);
  const transporter = nodemail.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: process.env.USER_NAME,
      pass : process.env.PASSWORD,
    }

   }); 
   
   let mailOptions = {
    from: 'uma.narasimhan@isita.com.mx', // sender address
    to: User.visitoremail,
    cc: User.orgpersonemail, // list of receivers
    subject: 'Email from Visitor Management', // Subject line
    text: 'Hello', // plain text body
    html: 'Below is your QRcode for the meeting: </br> <img src="' + Img + '">' // html body
};
transporter.sendMail(mailOptions, (error, info) => {
  if (error) {
      return console.log(error);
  }
  console.log('Message %s sent: %s', info.messageId, info.response);
  res.send("Email has been sent to your email account");
});

      }catch(err) {
    res.status(404).send(err);
    }
 

});

module.exports = router;
