const mongoose = require('mongoose');
const vmonlineuserSchema = new mongoose.Schema(
{
   visitorname:{
        type: String,
        required: true,
        min: 6,
        max: 255
      },
      visitoremail:{
        type: String,
        required: true,
        min: 6,
        max: 255
      },
      visitororgname:{
        type: String,
        require: true,
        min:2,
        max:255
      },

      visitormnumber:{
        type: String,
        required: true,
        min: 10,
        max:15
      },
     meetingstartdate:{
       type: String,
       
     },
     meetingenddate:{
         type: String,
     },

     orgpersonname:{
        type: String,
        required: true,
        min: 6,
        max: 255

     },

     orgpersonemail:{
        type: String,
        required: true,
        min: 6,
        max: 255 
     },
     department:{
         type: String,
         required: true,
         min:2,
         max:255
     },
     qrcode:{
      contentType: String,
      data: Buffer,
     },
   
      visitorimg:{
        type: String,
        data: Buffer,
        min:6,
        max:1024
      }
  
   } );




module.exports = mongoose.model('VMOnlineUser',vmonlineuserSchema);