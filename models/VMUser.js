const mongoose = require('mongoose');
const vmuserSchema =  new mongoose.Schema(
{
  visitorname:{
     type: String,
     required: true,
     min: 6,
     max: 255
   },
   vistoremail:{
     type: String,
     required: true,
     min: 6,
     max: 255
   },
   visitormnumber:{
    type: String,
    required: true,
    min: 10,
    max:15
  },

   visitororgname:{
     type: String,
     require: true,
     min:6,
     max:255
   },
   persontomeet:{
    type: String,
    required: true,
    min: 6,
    max: 255
   },
   image:{
     contentType: String,
     data: Buffer,
     
   },

  date:{
    type: Date,
    default: Date.now
  }

}

);
module.exports = mongoose.model('VMUser',vmuserSchema);

