const Joi = require('@hapi/joi');

// validation for user registration
const registerValidation = (data)=>{
  const schema ={
    visitorname: Joi.string().min(2).required(),
    vistoremail: Joi.string().email({ minDomainSegments: 2 }),
    visitororgname:Joi.string().min(1).required(),
    image:Joi.string().min(1).required(),
    visitormnumber:Joi.string().min(7).required(),
    persontomeet : Joi.string().min(2).required()
  };
  return Joi.validate(data,schema);
} ;



const onlineregisterValidation =(data) =>{
const schema ={
  visitorname : Joi.string().min(6).required(),
  visitoremail : Joi.string().email({ minDomainSegments: 2 }),
  visitororgname: Joi.string().min(2).required(),
  visitormnumber : Joi.string().min(10).required(),
  meetingstartdate : Joi.string().min(6).required(),
  meetingenddate : Joi.string().min(6).required(),
  orgpersonname : Joi.string().min(6).required(),
  orgpersonemail : Joi.string().email({ minDomainSegments: 2 }),
  department : Joi.string().min(2).required(),
  qrcode : Joi.string().min(2).required()
};
return Joi.validate(data,schema);
};
module.exports.registerValidation = registerValidation;
module.exports.onlineregisterValidation = onlineregisterValidation;
